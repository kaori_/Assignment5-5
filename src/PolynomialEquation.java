import javafx.css.Match;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PolynomialEquation {
    ArrayList<Item> polynomial;

    PolynomialEquation(String equation) {
        polynomial = new ArrayList<>();

        //Remove "F(x) ="
        String leftSide = equation.substring(equation.indexOf("=") + 1).trim();

        String[] items = leftSide.split("[\\-,+]+", 0);
        String[] nums;
        for (int i = 0; i < items.length; i++) {
            if (!Objects.equals(items[i], "")) {
                nums = items[i].trim().split("X\\^", 0);
                if (nums.length < 2) {
                    polynomial.add(new Item(Integer.parseInt(nums[0]), 0));
                } else if (Objects.equals(nums[0], "")) {
                    polynomial.add(new Item(1, Integer.parseInt(nums[1])));
                } else {
                    polynomial.add(new Item(Integer.parseInt(nums[0]), Integer.parseInt(nums[1])));
                }
            }
        }
    }

    public float quantity(double x) {
        float ans = 0;
        for (Item i : polynomial) {
            ans += i.calculate(x);
        }
        return ans;
    }

    public String derivative() {
        StringBuilder equation = new StringBuilder();
        for (Item i : polynomial) {
            equation.append(i.getDerivative());
        }

        String regex = "^\\s*\\+\\s*";
        equation = new StringBuilder(equation.toString().replaceAll(regex, ""));

        equation.insert(0, "F'(x) = ");
        return equation.toString();
    }
}
