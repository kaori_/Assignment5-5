public class Main {

    public static void main(String[] args) {
       String input = "F(x) = 3X^10 + 5X^6 + 4X^3+5";

       PolynomialEquation pe = new PolynomialEquation(input);
       System.out.print("F(5.6) = ");
       System.out.println(pe.quantity(5.6));

       String derivative = pe.derivative();
       System.out.println(derivative);

       PolynomialEquation ped = new PolynomialEquation(derivative);
       System.out.print("F'(5.6) = ");
       System.out.println(ped.quantity(5.6));

    }
}
