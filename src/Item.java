public class Item {
    private int coefficient;
    private int power;

    Item(int coefficient, int power) {
        this.coefficient = coefficient;
        this.power = power;
    }

    public double calculate(double x) {
        return this.coefficient * Math.pow(x, this.power);
    }

    public String getDerivative() {
        String itemDerivative = "";

        if (this.power == 0) {
            itemDerivative = "";
        } else if (this.power-1 == 0) {
            itemDerivative = " + " + String.valueOf((this.coefficient*this.power));
        } else {
            itemDerivative = " + " + String.valueOf((this.coefficient*this.power)) + "X^" + String.valueOf((this.power-1));
        }

        return itemDerivative;
    }

}
